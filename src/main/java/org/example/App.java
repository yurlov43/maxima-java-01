package org.example;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        int[] numbers = {15, 458, 1, 73, 105};
        System.out.println(maxDigitsSumPosition(numbers));
    }

    public static byte maxDigitsSumPosition(int[] arr)
    {
        byte maxIndex = 0;
        int maxSum = 0;

        for (int i = 0; i < arr.length; i++)
        {
            int number = arr[i];
            int sum = 0;

            while (number > 0)
            {
                sum = sum + number % 10;
                number = number / 10;
            }
            if (sum >= maxSum)
            {
                maxSum = sum;
                maxIndex = (byte) i;
            }
        }
        return maxIndex;
    }

    public static int isSimple(int n)
    {
        int divisor = 0;

        for (int i = 2; i <= Math.sqrt(n); i++)
        {
            if (n % i == 0)
            {
                divisor = i;
                break;
            }
        }
        return divisor;
    }

    public static byte daysCount(byte month, int year)
    {
        byte daysCount;

        switch (month)
        {
            case 4:
            case 6:
            case 9:
            case 11:
                daysCount = 30;
                break;
            case 2:
                daysCount = 28;
                if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
                {
                    daysCount = 29;
                }
                break;
            default:
                daysCount = 31;
                break;
        }
        return daysCount;
    }

    public static long fibonazzi( byte number )
    {
        return number <= 1
                ? number
                : fibonazzi((byte) (number - 2)) + fibonazzi((byte) (number - 1));
    }

    public static void swap()
    {
        char a = '\u2665';
        char b = '\u2663';
        char c;

        System.out.println(a + " " + b);
        c = a;
        a = b;
        b = c;
        System.out.println(a + " " + b);
    }

    public static void print(String str)
    {
        System.out.println(str);
    }
}
